"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_redux_1 = require("react-redux");
var react_router_dom_1 = require("react-router-dom");
var SamplesStore = require("../store/Samples");
var OrderSample = /** @class */ (function (_super) {
    __extends(OrderSample, _super);
    function OrderSample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // This method is called when the component is first added to the document
    OrderSample.prototype.componentDidMount = function () {
        this.ensureDataFetched();
    };
    // This method is called when the route parameters change
    OrderSample.prototype.componentDidUpdate = function () {
        this.ensureDataFetched();
    };
    OrderSample.prototype.render = function () {
        return (React.createElement(React.Fragment, null,
            React.createElement("h1", { id: "tabelLabel" }, "Samples"),
            React.createElement("p", null, "This component demonstrates fetching data from the server and working with URL parameters."),
            this.renderSamplesTable(),
            this.renderPagination()));
    };
    OrderSample.prototype.ensureDataFetched = function () {
        var startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
        this.props.requestSamples(startDateIndex);
    };
    OrderSample.prototype.renderSamplesTable = function () {
        return (React.createElement("table", { className: 'table table-striped', "aria-labelledby": "tabelLabel" },
            React.createElement("thead", null,
                React.createElement("tr", null,
                    React.createElement("th", null, "F\u00F6retag"),
                    React.createElement("th", null, "Epost"),
                    React.createElement("th", null, "Team"),
                    React.createElement("th", null, "Status"))),
            React.createElement("tbody", null, this.props.samples.map(function (sample) {
                return React.createElement("tr", { key: sample.firstName },
                    React.createElement("td", null, sample.firstName),
                    React.createElement("td", null, sample.lastName),
                    React.createElement("td", null, sample.email),
                    React.createElement("td", null, sample.email));
            }))));
    };
    OrderSample.prototype.renderPagination = function () {
        var prevStartDateIndex = (this.props.startDateIndex || 0) - 5;
        var nextStartDateIndex = (this.props.startDateIndex || 0) + 5;
        return (React.createElement("div", { className: "d-flex justify-content-between" },
            React.createElement(react_router_dom_1.Link, { className: 'btn btn-outline-secondary btn-sm', to: "/sample/" + prevStartDateIndex }, "Previous"),
            this.props.isLoading && React.createElement("span", null, "Loading..."),
            React.createElement(react_router_dom_1.Link, { className: 'btn btn-outline-secondary btn-sm', to: "/sample/" + nextStartDateIndex }, "Next")));
    };
    return OrderSample;
}(React.PureComponent));
exports.default = react_redux_1.connect(function (state) { return state.samples; }, // Selects which state properties are merged into the component's props
SamplesStore.actionCreators // Selects which action creators are merged into the component's props
)(OrderSample);
//# sourceMappingURL=OrderSample.js.map