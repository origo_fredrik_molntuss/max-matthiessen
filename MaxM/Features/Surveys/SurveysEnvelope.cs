﻿using System.Collections.Generic;
using MaxM.Domain.Entities;

namespace MaxM.Features.Surveys
{
    public class SurveysEnvelope
    {
        public List<Survey> Surveys { get; set; }
        public int SurveysCount { get; set; }
    }
}
