﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Companies
{
    public class Edit
    {
        public class CompanyData
        {
            public string Name { get; set; }
            public string OrganizationNumber { get; set; }
            public string Email { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class Command : IRequest<CompanyEnvelope>
        {
            public CompanyData Company { get; set; }
            public int CompanyId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Company).NotNull();
            }
        }

        public class Handler : IRequestHandler<Command, CompanyEnvelope>
        {
            private readonly MaxMContext _context;

            public Handler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<CompanyEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var company = await _context.Companies
                    .Where(x => x.CompanyId == message.CompanyId)
                    .FirstOrDefaultAsync(cancellationToken);

                if (company == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Sample = Constants.NOT_FOUND });
                }

                company.Name = message.Company.Name ?? company.Name;
                company.OrganizationNumber = message.Company.OrganizationNumber ?? company.OrganizationNumber;
                company.Email = message.Company.Email ?? company.Email;

                if (_context.ChangeTracker.Entries().First(x => x.Entity == company).State == EntityState.Modified)
                {
                    company.UpdatedAt = DateTime.UtcNow;
                }

                await _context.SaveChangesAsync(cancellationToken);

                return new CompanyEnvelope(await _context.Companies
                    .Where(x => x.CompanyId == company.CompanyId)
                    .FirstOrDefaultAsync(cancellationToken));
            }
        }
    }
}
