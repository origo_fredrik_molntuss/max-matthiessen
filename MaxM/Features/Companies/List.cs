﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Companies
{
    public class List
    {
        public class Query : IRequest<CompaniesEnvelope>
        {
            public Query(int? teamId, int? limit, int? offset)
            {
                TeamId = teamId;
                Limit = limit;
                Offset = offset;
            }

            public int? TeamId { get; set; }
            public int? Limit { get; }
            public int? Offset { get;  }
        }

        public class QueryHandler : IRequestHandler<Query, CompaniesEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<CompaniesEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<Company> queryable = _context.Companies;

                if (_currentUserAccessor.GetCurrentUsername() != null)
                {
                    var currentUser = await _context.Advisors.FirstOrDefaultAsync(x => x.Username == _currentUserAccessor.GetCurrentUsername(), cancellationToken);
                    queryable = queryable.Where(x => x.Team == currentUser.Team);
                }

                var companies = await queryable
                    .OrderBy(x => x.Name)
                    .Skip(message.Offset ?? 0)
                    .Take(message.Limit ?? 20)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new CompaniesEnvelope()
                {
                    Companies = companies,
                    CompaniesCount = companies.Count()
                };
            }
        }
    }
}
