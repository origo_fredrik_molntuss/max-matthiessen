﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Results
{
    public class Details
    {
        public class Query : IRequest<ResultEnvelope>
        {
            public Query(int resultId)
            {
                ResultId = resultId;
            }
            public int ResultId { get; set; }
        }

        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.ResultId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Query, ResultEnvelope>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<ResultEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var result = await _context.Results
                    .FirstOrDefaultAsync(x => x.ResultId == message.ResultId, cancellationToken);

                if (result == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Result = Constants.NOT_FOUND });
                }

                return new ResultEnvelope(result);
            }
        }
    }
}
