﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Microsoft.EntityFrameworkCore;
using MaxM.Features.Advisors;
using MaxM.IntegrationTests.Features.Users;
using System;

namespace MaxM.IntegrationTests.Features.Advisors
{
    public class DeleteTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Delete_Advisor()
        {
            var createCmd = new Create.Command()
            {
                Advisor = new Create.AdvisorData()
                {
                    FirstName = "Teste",
                    LastName = "Testesson",
                    Email = "test@test.com",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }
            };

            var advisor = await AdvisorHelpers.CreateAdvisor(this, createCmd);

            var deleteCmd = new Delete.Command(advisor.AdvisorId);

            var dbContext = GetDbContext();

            var statusDeleteHandler = new Delete.QueryHandler(dbContext);
            await statusDeleteHandler.Handle(deleteCmd, new System.Threading.CancellationToken());

            var dbStatus = await ExecuteDbContextAsync(db => db.Advisors.Where(d => d.AdvisorId == deleteCmd.AdvisorId).SingleOrDefaultAsync());

            Assert.Null(dbStatus);
        }
    }
}
