﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Results
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Command(int resultId)
            {
                ResultId = resultId;
            }
            public int ResultId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.ResultId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Command>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command message, CancellationToken cancellationToken)
            {
                var result = await _context.Results.FirstOrDefaultAsync(x => x.ResultId == message.ResultId, cancellationToken);

                if (result == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Result = Constants.NOT_FOUND });
                }

                _context.Results.Remove(result);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
