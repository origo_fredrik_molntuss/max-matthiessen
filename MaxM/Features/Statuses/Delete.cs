﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Statuses
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Command(int statusId)
            {
                StatusId = statusId;
            }
            public int StatusId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.StatusId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Command>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command message, CancellationToken cancellationToken)
            {
                var status = await _context.Statuses.FirstOrDefaultAsync(x => x.StatusId == message.StatusId, cancellationToken);

                if (status == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Status = Constants.NOT_FOUND });
                }

                _context.Statuses.Remove(status);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
