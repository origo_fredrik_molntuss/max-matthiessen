﻿using MaxM.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.EntityFrameworkCore.Storage;
using System.Data;
using System.Threading.Tasks;

namespace MaxM.Infrastructure
{
    public class MaxMContext : DbContext
    {
        private IDbContextTransaction _currentTransaction;

        public MaxMContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Advisor> Advisors { get; set; }
        public DbSet<BusinessArea> BusinessAreas { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Result> Results { get; set; }
        public DbSet<Segment> Segments { get; set; }
        public DbSet<Sample> Samples { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Team> Teams { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
               .HasDbFunction(typeof(MaxMContext).GetMethod(nameof(Soundex)))
               .HasTranslation(
                   args =>
                       SqlFunctionExpression.Create("SOUNDEX", args, typeof(int?), null));
        }

        #region Transaction Handling
        public void BeginTransaction()
        {
            if (_currentTransaction != null)
            {
                return;
            }

            if (!Database.IsInMemory())
            {
                _currentTransaction = Database.BeginTransaction(IsolationLevel.ReadCommitted);
            }
        }

        public void CommitTransaction()
        {
            try
            {
                _currentTransaction?.Commit();
            }
            catch
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                _currentTransaction?.Rollback();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }
        #endregion

        [DbFunction("SOUNDEX")]
        public static string Soundex(string s) => throw new System.Exception();
    }
}
