﻿using MaxM.Domain.Entities;

namespace MaxM.Features.BusinessAreas
{
    public class BusinessAreaEnvelope
    {
        public BusinessArea BusinessArea { get; }

        public BusinessAreaEnvelope(BusinessArea businessArea)
        {
            BusinessArea = businessArea;
        }
    }
}
