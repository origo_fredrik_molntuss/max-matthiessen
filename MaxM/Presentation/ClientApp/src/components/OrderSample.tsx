﻿import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApplicationState } from '../store';
import * as SamplesStore from '../store/Samples';
import { Formik, Field, Form, FormikActions } from 'formik';

// At runtime, Redux will merge together...
type SampleProps =
    SamplesStore.SamplesState // ... state we've requested from the Redux store
    & typeof SamplesStore.actionCreators // ... plus action creators we've requested
    & RouteComponentProps<{ startDateIndex: string }>; // ... plus incoming routing parameters


class OrderSample extends React.PureComponent<SampleProps> {
    // This method is called when the component is first added to the document
    public componentDidMount() {
        this.ensureDataFetched();
    }

    // This method is called when the route parameters change
    public componentDidUpdate() {
        this.ensureDataFetched();
    }

    public render() {
        return (
            <React.Fragment>
                <h1 id="tabelLabel">Best&auml;ll</h1>
                {this.renderSamplesTable()}
                {this.renderPagination()}
            </React.Fragment>
        );
    }

    private ensureDataFetched() {
        const startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
        this.props.requestSamples(startDateIndex);
    }

    private renderSamplesTable() {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Företag</th>
                        <th>Epost</th>
                        <th>Team</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.samples.map((sample: SamplesStore.Sample) =>
                        <tr key={sample.email}>
                            <td>{sample.company}</td>
                            <td>{sample.status}</td>
                            <td>{sample.email}</td>
                            <td>{sample.answered}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    private renderPagination() {
        const prevStartDateIndex = (this.props.startDateIndex || 0) - 5;
        const nextStartDateIndex = (this.props.startDateIndex || 0) + 5;

        return (
            <div className="d-flex justify-content-between">
                <Link className='btn btn-outline-secondary btn-sm' to={`/sample/${prevStartDateIndex}`}>Previous</Link>
                {this.props.isLoading && <span>Laddar</span>}
                <Link className='btn btn-outline-secondary btn-sm' to={`/sample/${nextStartDateIndex}`}>Next</Link>
            </div>
        );
    }
}

export default connect(
    (state: ApplicationState) => state.samples, // Selects which state properties are merged into the component's props
    SamplesStore.actionCreators // Selects which action creators are merged into the component's props
)(OrderSample as any);
