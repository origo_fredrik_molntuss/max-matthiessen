﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MaxM.IntegrationTests.Features.Users;
using MaxM.Features.Companies;

namespace MaxM.IntegrationTests.Features.Companies
{
    public static class CompanyHelpers
    {
        /// <summary>
        /// creates a company based on the given Create command. It also creates a default user.
        /// </summary>
        /// <param name="fixture"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public static async Task<MaxM.Domain.Entities.Company> CreateCompany(SliceFixture fixture, Create.Command command)
        {
            // first create the default user
            var user = await UserHelpers.CreateDefaultUser(fixture);

            var dbContext = fixture.GetDbContext();
            var currentAccessor = new StubCurrentUserAccessor(user.Username);

            var sampleCreateHandler = new Create.Handler(dbContext, currentAccessor);
            var created = await sampleCreateHandler.Handle(command, new System.Threading.CancellationToken());
            var dbSample = await fixture.ExecuteDbContextAsync(db => db.Companies.Where(c => c.CompanyId == created.Company.CompanyId)
                .SingleOrDefaultAsync());

            return dbSample;
        }
    }
}
