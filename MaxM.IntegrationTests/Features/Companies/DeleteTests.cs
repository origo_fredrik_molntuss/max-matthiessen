﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Microsoft.EntityFrameworkCore;
using MaxM.Features.Companies;
using MaxM.IntegrationTests.Features.Users;
using System;

namespace MaxM.IntegrationTests.Features.Companies
{
    public class DeleteTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Delete_Company()
        {
            var createCmd = new Create.Command()
            {
                Company = new Create.CompanyData()
                {
                    Name = "Test",
                    OrganizationNumber = "123456789",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }
            };

            var company = await CompanyHelpers.CreateCompany(this, createCmd);

            var deleteCmd = new Delete.Command(company.CompanyId);

            var dbContext = GetDbContext();

            var companyDeleteHandler = new Delete.QueryHandler(dbContext);
            await companyDeleteHandler.Handle(deleteCmd, new System.Threading.CancellationToken());

            var dbCompany = await ExecuteDbContextAsync(db => db.Companies.Where(d => d.CompanyId == deleteCmd.CompanyId).SingleOrDefaultAsync());

            Assert.Null(dbCompany);
        }
    }
}
