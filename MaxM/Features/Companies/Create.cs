﻿using FluentValidation;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MaxM.Features.Companies
{
    public class Create
    {
        public class CompanyData
        {
            public string Name { get; set; }
            public string OrganizationNumber { get; set; }
            public string Email { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class CompanyDataValidator : AbstractValidator<CompanyData>
        {
            public CompanyDataValidator()
            {
                RuleFor(x => x.Name).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<CompanyEnvelope>
        {
            public CompanyData Company { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Company).NotNull().SetValidator(new CompanyDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, CompanyEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public Handler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<CompanyEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var company = new Company()
                {
                    Name = message.Company.Name,
                };

                await _context.Companies.AddAsync(company, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return new CompanyEnvelope(company);
            }
        }
    }
}
