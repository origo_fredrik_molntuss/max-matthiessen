using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Samples
{
    public class List
    {
        public class Query : IRequest<SamplesEnvelope>
        {
            public Query(int? companyId, int? statusId, int? limit, int? offset)
            {
                CompanyId = companyId;
                StatusId = statusId;
                Limit = limit;
                Offset = offset;
            }

            public int? StatusId { get; set; }
            public int? CompanyId { get; set; }
            public int? Limit { get; }
            public int? Offset { get; }
            public bool IsFeed { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, SamplesEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;
            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<SamplesEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<Sample> queryable = _context.Samples.GetAllData();

                if (_currentUserAccessor.GetCurrentUsername() != null)
                {
                    var currentUser = await _context.Advisors.FirstOrDefaultAsync(x => x.Username == _currentUserAccessor.GetCurrentUsername(), cancellationToken);
                    queryable = queryable.Where(x => x.Advisor.AdvisorId == currentUser.AdvisorId);
                }

                if (message.CompanyId != null)
                {
                    var company = await _context.Companies.FirstOrDefaultAsync(x => x.CompanyId == message.CompanyId, cancellationToken);
                    if (company != null)
                    {
                        queryable = queryable.Where(x => x.Company == company);
                    }
                }

                if (message.StatusId != null)
                {
                    var status = await _context.Statuses.FirstOrDefaultAsync(x => x.StatusId == message.StatusId, cancellationToken);
                    if (status != null)
                    {
                        queryable = queryable.Where(x => x.Status == status);
                    }
                }

                var samples = await queryable
                    .OrderByDescending(x => x.CreatedAt)
                    .Skip(message.Offset ?? 0)
                    .Take(message.Limit ?? 20)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new SamplesEnvelope()
                {
                    Samples = samples,
                    SamplesCount = samples.Count()
                };
            }
        }
    }
}