﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MaxM.Features.Samples;
using MaxM.Infrastructure;
using MaxM.IntegrationTests;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace MaxM.IntegrationTests.Features.Samples
{
    public class EditTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Edit_Sample()
        {
            var companyCommand = new MaxM.Features.Companies.Create.Command()
            {
                Company = new MaxM.Features.Companies.Create.CompanyData()
                {
                    Name = "Teste",
                    OrganizationNumber = "123456789",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }

            };

            var company = await SampleHelpers.CreateCompany(this, companyCommand);

            var statusCommand = new MaxM.Features.Statuses.Create.Command()
            {
                Status = new MaxM.Features.Statuses.Create.StatusData()
                {
                    Name = "Besvarad"
                }

            };

            var status = await SampleHelpers.CreateStatus(this, statusCommand);

            var surveyCommand = new MaxM.Features.Surveys.Create.Command()
            {
                Survey = new MaxM.Features.Surveys.Create.SurveyData()
                {
                    Name = "Enkät Ett"
                }

            };

            var survey = await SampleHelpers.CreateSurvey(this, surveyCommand);

            var sampleCommand = new Create.Command()
            {
                Sample = new Create.SampleData()
                {
                    FirstName = "Teste",
                    LastName = "Testesson",
                    Email = "test@test.com",
                    CompanyId = company.CompanyId,
                    StatusId = status.StatusId,
                    SurveyId = survey.SurveyId,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }

            };

            var sample = await SampleHelpers.CreateSample(this, sampleCommand);

            var command = new Edit.Command()
            {
                Sample = new Edit.SampleData()
                {
                    FirstName = "Testelina",
                    LastName = "Testesson",
                    Email = "teste@teste.com",
                    CompanyId = company.CompanyId,
                    StatusId = status.StatusId,
                    SurveyId = survey.SurveyId,
                    UpdatedAt = DateTime.Now
                },
                SampleId = sample.SampleId
            };
            // remove the first tag and add a new tag

            var dbContext = GetDbContext();

            var sampleEditHandler = new Edit.Handler(dbContext);
            var edited = await sampleEditHandler.Handle(command, new System.Threading.CancellationToken());

            Assert.NotNull(edited);
            Assert.Equal(edited.Sample.FirstName, command.Sample.FirstName);
        }
    }
}
