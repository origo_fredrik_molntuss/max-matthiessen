﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MaxM.Features.Advisors;
using MaxM.Infrastructure;
using MaxM.IntegrationTests;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace MaxM.IntegrationTests.Features.Advisors
{
    public class EditTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Edit_Advisor()
        {
            var createCommand = new Create.Command()
            {
                Advisor = new Create.AdvisorData()
                {
                    FirstName = "Teste",
                    LastName = "Testesson",
                    Email = "test@test.com",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }
            };

            var createdAdvisor = await AdvisorHelpers.CreateAdvisor(this, createCommand);

            var command = new Edit.Command()
            {
                Advisor = new Edit.AdvisorData()
                {
                    FirstName = "Teste",
                    LastName = "Testesson",
                    Email = "test@test.com",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                },
                AdvisorId = createdAdvisor.AdvisorId
            };

            var dbContext = GetDbContext();

            var advisorEditHandler = new Edit.Handler(dbContext);
            var edited = await advisorEditHandler.Handle(command, new System.Threading.CancellationToken());

            Assert.NotNull(edited);
            Assert.Equal(edited.Advisor.FirstName, command.Advisor.FirstName);
        }
    }
}
