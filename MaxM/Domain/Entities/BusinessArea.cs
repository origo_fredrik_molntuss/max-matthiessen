﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaxM.Domain.Entities
{
    public class BusinessArea
    {
        public int BusinessAreaId { get; set; }
        public string Name { get; set; }
    }
}
