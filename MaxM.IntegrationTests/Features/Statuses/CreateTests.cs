using System.Threading.Tasks;
using MaxM.Features.Statuses;
using Xunit;

namespace MaxM.IntegrationTests.Features.Statuses
{
    public class CreateTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Create_Statuses()
        {

            var command = new Create.Command()
            {
                Status = new Create.StatusData()
                {
                    Name = "Teste"
                }

            };

            var status = await StatusHelpers.CreateStatus(this, command);

            Assert.NotNull(status);
            Assert.Equal(status.Name, command.Status.Name);
        }
    }
}