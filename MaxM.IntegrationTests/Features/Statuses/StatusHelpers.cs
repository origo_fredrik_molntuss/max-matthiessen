﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MaxM.IntegrationTests.Features.Users;

namespace MaxM.IntegrationTests.Features.Statuses
{
    public static class StatusHelpers
    {
        /// <summary>
        /// creates a status based on the given Create command. It also creates a default user
        /// </summary>
        /// <param name="fixture"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public static async Task<MaxM.Domain.Entities.Status> CreateStatus(SliceFixture fixture, MaxM.Features.Statuses.Create.Command command)
        {
            // first create the default user
            var user = await UserHelpers.CreateDefaultUser(fixture);

            var dbContext = fixture.GetDbContext();
            var currentAccessor = new StubCurrentUserAccessor(user.Username);

            var statusCreateHandler = new MaxM.Features.Statuses.Create.Handler(dbContext, currentAccessor);
            var created = await statusCreateHandler.Handle(command, new System.Threading.CancellationToken());

            var dbSample = await fixture.ExecuteDbContextAsync(db => db.Statuses.Where(s => s.StatusId == created.Status.StatusId)
                .SingleOrDefaultAsync());

            return dbSample;
        }
    }
}
