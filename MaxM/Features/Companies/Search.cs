﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Companies
{
    public class Search
    {
        public class Query : IRequest<CompaniesEnvelope>
        {
            public Query(string searchTerm)
            {
                SearchTerm = searchTerm;
            }

            public string SearchTerm { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, CompaniesEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<CompaniesEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<Company> queryable = _context.Companies;

                if (_currentUserAccessor.GetCurrentUsername() != null)
                {
                    var currentUser = await _context.Advisors.FirstOrDefaultAsync(x => x.Username == _currentUserAccessor.GetCurrentUsername(), cancellationToken);
                    queryable = queryable.Where(x => x.Team == currentUser.Team);
                }

                var companies = await queryable
                    //.Where(x => MaxMContext.Soundex(x.Name) == MaxMContext.Soundex(message.SearchTerm))
                    .Where(x => x.Name.ToLower().Contains(message.SearchTerm.ToLower()))
                    .OrderBy(x => x.Name)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new CompaniesEnvelope()
                {
                    Companies = companies,
                    CompaniesCount = companies.Count()
                };
            }
        }
    }
}
