﻿using System.Collections.Generic;
using MaxM.Domain.Entities;

namespace MaxM.Features.Results
{
    public class ResultsEnvelope
    {
        public List<Result> Results { get; set; }
        public int ResultsCount { get; set; }
    }
}
