﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaxM.Domain.Entities
{
    public class Result
    {
        public int ResultId { get; set; }
        public string Text { get; set; }
        public int? Value { get; set; }
        public Question Question { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
