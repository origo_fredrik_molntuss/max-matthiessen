﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Surveys
{
    public class Details
    {
        public class Query : IRequest<SurveyEnvelope>
        {
            public Query(int surveyId)
            {
                SurveyId = surveyId;
            }
            public int SurveyId { get; set; }
        }

        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.SurveyId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Query, SurveyEnvelope>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<SurveyEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var survey = await _context.Surveys
                    .FirstOrDefaultAsync(x => x.SurveyId == message.SurveyId, cancellationToken);

                if (survey == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Survey = Constants.NOT_FOUND });
                }

                return new SurveyEnvelope(survey);
            }
        }
    }
}
