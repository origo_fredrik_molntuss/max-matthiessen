﻿using System.Threading.Tasks;
using MaxM.Infrastructure.Security;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MaxM.Features.Statuses
{
    [Route("api/surveys")]
    public class SurveysController : Controller
    {
        private readonly IMediator _mediator;

        public SurveysController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<StatusesEnvelope> Get([FromQuery] int? teamId, [FromQuery] int? limit, [FromQuery] int? offset)
        {
            return await _mediator.Send(new List.Query(teamId, limit, offset));
        }

        [HttpGet("{statusId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<StatusEnvelope> Get(int statusId)
        {
            return await _mediator.Send(new Details.Query(statusId));
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<StatusEnvelope> Create([FromBody]Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("{statusId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<StatusEnvelope> Edit(int statusId, [FromBody]Edit.Command command)
        {
            command.StatusId = statusId;
            return await _mediator.Send(command);
        }

        [HttpDelete("{statusId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task Delete(int statusId)
        {
            await _mediator.Send(new Delete.Command(statusId));
        }
    }
}