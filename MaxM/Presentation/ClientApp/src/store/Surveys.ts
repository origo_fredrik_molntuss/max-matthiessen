﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface SurveysState {
    isLoading: boolean;
    startDateIndex?: number;
    surveys: Survey[];
}

export interface Survey {
    date: string;
    name: string;
    description: string;
    summary: string;
    result: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestSurveysAction {
    type: 'REQUEST_SURVEYS';
    startDateIndex: number;
}

interface RecieveSurveysAction {
    type: 'RECEIVE_SURVEYS';
    startDateIndex: number;
    surveys: Survey[];
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestSurveysAction | RecieveSurveysAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestSurveys: (startDateIndex: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.surveys && startDateIndex !== appState.surveys.startDateIndex) {
            fetch(`survey`)
                .then(response => response.json() as Promise<Survey[]>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_SURVEYS', startDateIndex: startDateIndex, surveys: data });
                });

            dispatch({ type: 'REQUEST_SURVEYS', startDateIndex: startDateIndex });
        }
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: SurveysState = { surveys: [], isLoading: false };

export const reducer: Reducer<SurveysState> = (state: SurveysState | undefined, incomingAction: Action): SurveysState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_SURVEYS':
            return {
                startDateIndex: action.startDateIndex,
                surveys: state.surveys,
                isLoading: true
            };
        case 'RECEIVE_SURVEYS':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.startDateIndex === state.startDateIndex) {
                return {
                    startDateIndex: action.startDateIndex,
                    surveys: action.surveys,
                    isLoading: false
                };
            }
            break;
    }

    return state;
};
