using MaxM.Domain.Entities;

namespace MaxM.Features.Samples
{
    public class SampleEnvelope
    {
        public Sample Sample { get; }

        public SampleEnvelope(Sample sample)
        {
            Sample = sample;
        }

    }
}