﻿using MaxM.Domain.Entities;

namespace MaxM.Features.Results
{
    public class ResultEnvelope
    {
        public Result Result { get; }

        public ResultEnvelope(Result result)
        {
            Result = result;
        }
    }
}
