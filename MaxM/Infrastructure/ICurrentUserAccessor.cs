﻿namespace MaxM.Infrastructure
{
    public interface ICurrentUserAccessor
    {
        string GetCurrentUsername();
    }
}