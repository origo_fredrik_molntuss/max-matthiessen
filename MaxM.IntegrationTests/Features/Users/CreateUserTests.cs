﻿using System.Linq;
using System.Threading.Tasks;
using MaxM.Features.Users;
using MaxM.Infrastructure.Security;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace MaxM.IntegrationTests.Features.Users
{
    public class CreateUserTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Create_User()
        {
            var command = new MaxM.Features.Users.Create.Command()
            {
                User = new Create.UserData()
                {
                    Email = "email",
                    Password = "password",
                    Username = "username"
                }
            };

            await SendAsync(command);

            var created = await ExecuteDbContextAsync(db => db.Advisors.Where(d => d.Email == command.User.Email).SingleOrDefaultAsync());

            Assert.NotNull(created);
            Assert.Equal(created.Hash, new PasswordHasher().Hash("password", created.Salt));
        }
    }
}
