﻿using MaxM.Domain.Entities;

namespace MaxM.Features.Segments
{
    public class SegmentEnvelope
    {
        public Segment Segment { get; }

        public SegmentEnvelope(Segment segment)
        {
            Segment = segment;
        }
    }
}
