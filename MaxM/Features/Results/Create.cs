﻿using FluentValidation;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MaxM.Features.Results
{
    public class Create
    {
        public class ResultData
        {
            public string Text { get; set; }
        }

        public class ResultDataValidator : AbstractValidator<ResultData>
        {
            public ResultDataValidator()
            {
                RuleFor(x => x.Text).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<ResultEnvelope>
        {
            public ResultData Result { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Result).NotNull().SetValidator(new ResultDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, ResultEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public Handler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<ResultEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var result = new Result()
                {
                    Text = message.Result.Text,
                };

                await _context.Results.AddAsync(result, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return new ResultEnvelope(result);
            }
        }
    }
}
