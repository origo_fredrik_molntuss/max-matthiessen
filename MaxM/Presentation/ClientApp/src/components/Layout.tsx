import * as React from 'react';
import { Link } from "react-router-dom";
import { Container } from 'reactstrap';
import NavMenu from './NavMenu';
import './Layout.css';

export default (props: { children?: React.ReactNode }) => (
    <React.Fragment>
        <NavMenu />
        <div className="gradient coverflow mb-4">
            <ul>
                <li>
                    <Link to="/sample" className="btn-order" title="Best&auml;ll enk&auml;ter">Best&auml;ll</Link>
                </li>
                <li>
                    <Link to="/act" className="btn-act" title="Hitta styrkor och svagheter">Agera</Link>
                </li>
                <li>
                    <Link to="/analyze" className="btn-analyze" title="Granska resultatet">Analysera</Link>
                </li>
            </ul>
        </div>
        <Container>
            {props.children}
        </Container>
    </React.Fragment>
);
