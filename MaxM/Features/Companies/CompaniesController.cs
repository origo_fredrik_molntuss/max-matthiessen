﻿using System.Threading.Tasks;
using MaxM.Infrastructure.Security;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MaxM.Features.Companies
{
    [Route("api/companies")]
    public class CompaniesController : Controller
    {
        private readonly IMediator _mediator;

        public CompaniesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<CompaniesEnvelope> Get([FromQuery] int? teamId, [FromQuery] int? limit, [FromQuery] int? offset)
        {
            return await _mediator.Send(new List.Query(teamId, limit, offset));
        }

        [HttpGet]
        [Route("search")]
        public async Task<CompaniesEnvelope> Get([FromQuery] string searchTerm)
        {
            return await _mediator.Send(new Search.Query(searchTerm));
        }

        [HttpGet("{companyId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<CompanyEnvelope> Get(int companyId)
        {
            return await _mediator.Send(new Details.Query(companyId));
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<CompanyEnvelope> Create([FromBody]Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("{companyId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<CompanyEnvelope> Edit(int companyId, [FromBody]Edit.Command command)
        {
            command.CompanyId = companyId;
            return await _mediator.Send(command);
        }

        [HttpDelete("{companyId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task Delete(int companyId)
        {
            await _mediator.Send(new Delete.Command(companyId));
        }
    }
}