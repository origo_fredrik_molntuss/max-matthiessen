﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MaxM.IntegrationTests.Features.Users;
using AutoMapper;
using MaxM.Features.Users;

namespace MaxM.IntegrationTests.Features.Samples
{
    public static class SampleHelpers
    {
        /// <summary>
        /// creates a sample based on the given Create command. It also creates a default user
        /// </summary>
        /// <param name="fixture"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public static async Task<MaxM.Domain.Entities.Sample> CreateSample(SliceFixture fixture, MaxM.Features.Samples.Create.Command command)
        {
            // first create the default user
            var advisor = await fixture.ExecuteDbContextAsync(db => db.Advisors.Where(a => a.AdvisorId == 1).SingleOrDefaultAsync());
            IMapper mapper = new MapperConfiguration(c => c.AddProfile<MappingProfile>()).CreateMapper();
            var user = mapper.Map<MaxM.Domain.Entities.Advisor, MaxM.Features.Users.User>(advisor);
            if (user == null)
            {
                user = await UserHelpers.CreateDefaultUser(fixture);
            }

            var dbContext = fixture.GetDbContext();
            var currentAccessor = new StubCurrentUserAccessor(user.Username);

            var sampleCreateHandler = new MaxM.Features.Samples.Create.Handler(dbContext, currentAccessor);
            var created = await sampleCreateHandler.Handle(command, new System.Threading.CancellationToken());

            var dbSample = await fixture.ExecuteDbContextAsync(db => db.Samples.Where(s => s.SampleId == created.Sample.SampleId)
                .SingleOrDefaultAsync());

            return dbSample;
        }
        public static async Task<MaxM.Domain.Entities.Company> CreateCompany(SliceFixture fixture, MaxM.Features.Companies.Create.Command command)
        {
            // first create the default user
            var advisor = await fixture.ExecuteDbContextAsync(db => db.Advisors.Where(a => a.AdvisorId == 1).SingleOrDefaultAsync());
            IMapper mapper = new MapperConfiguration(c => c.AddProfile<MappingProfile>()).CreateMapper();
            var user = mapper.Map<MaxM.Domain.Entities.Advisor, MaxM.Features.Users.User>(advisor);
            if (user == null)
            {
                user = await UserHelpers.CreateDefaultUser(fixture);
            }

            var dbContext = fixture.GetDbContext();
            var currentAccessor = new StubCurrentUserAccessor(user.Username);

            var companyCreateHandler = new MaxM.Features.Companies.Create.Handler(dbContext, currentAccessor);
            var created = await companyCreateHandler.Handle(command, new System.Threading.CancellationToken());

            var dbCompany = await fixture.ExecuteDbContextAsync(db => db.Companies.Where(c => c.CompanyId == created.Company.CompanyId)
                .SingleOrDefaultAsync());

            return dbCompany;
        }
        public static async Task<MaxM.Domain.Entities.Status> CreateStatus(SliceFixture fixture, MaxM.Features.Statuses.Create.Command command)
        {
            // first create the default user
            var advisor = await fixture.ExecuteDbContextAsync(db => db.Advisors.Where(a => a.AdvisorId == 1).SingleOrDefaultAsync());
            IMapper mapper = new MapperConfiguration(c => c.AddProfile<MappingProfile>()).CreateMapper();
            var user = mapper.Map<MaxM.Domain.Entities.Advisor, MaxM.Features.Users.User>(advisor);
            if (user == null)
            {
                user = await UserHelpers.CreateDefaultUser(fixture);
            }

            var dbContext = fixture.GetDbContext();
            var currentAccessor = new StubCurrentUserAccessor(user.Username);

            var statusCreateHandler = new MaxM.Features.Statuses.Create.Handler(dbContext, currentAccessor);
            var created = await statusCreateHandler.Handle(command, new System.Threading.CancellationToken());

            var dbStatus = await fixture.ExecuteDbContextAsync(db => db.Statuses.Where(c => c.StatusId == created.Status.StatusId)
                .SingleOrDefaultAsync());

            return dbStatus;
        }
        public static async Task<MaxM.Domain.Entities.Survey> CreateSurvey(SliceFixture fixture, MaxM.Features.Surveys.Create.Command command)
        {
            // first create the default user
            var advisor = await fixture.ExecuteDbContextAsync(db => db.Advisors.Where(a => a.AdvisorId == 1).SingleOrDefaultAsync());
            IMapper mapper = new MapperConfiguration(c => c.AddProfile<MappingProfile>()).CreateMapper();
            var user = mapper.Map<MaxM.Domain.Entities.Advisor, MaxM.Features.Users.User>(advisor);
            if (user == null)
            {
                user = await UserHelpers.CreateDefaultUser(fixture);
            }

            var dbContext = fixture.GetDbContext();
            var currentAccessor = new StubCurrentUserAccessor(user.Username);

            var surveyCreateHandler = new MaxM.Features.Surveys.Create.Handler(dbContext, currentAccessor);
            var created = await surveyCreateHandler.Handle(command, new System.Threading.CancellationToken());

            var dbStatus = await fixture.ExecuteDbContextAsync(db => db.Surveys.Where(c => c.SurveyId == created.Survey.SurveyId)
                .SingleOrDefaultAsync());

            return dbStatus;
        }
    }
}
