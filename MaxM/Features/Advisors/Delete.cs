﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Advisors
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Command(int advisorId)
            {
                AdvisorId = advisorId;
            }
            public int AdvisorId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.AdvisorId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Command>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command message, CancellationToken cancellationToken)
            {
                var advisor = await _context.Advisors.FirstOrDefaultAsync(x => x.AdvisorId == message.AdvisorId, cancellationToken);

                if (advisor == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Advisor = Constants.NOT_FOUND });
                }

                _context.Advisors.Remove(advisor);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
