﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Surveys
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Command(int surveyId)
            {
                SurveyId = surveyId;
            }
            public int SurveyId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.SurveyId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Command>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command message, CancellationToken cancellationToken)
            {
                var survey = await _context.Surveys.FirstOrDefaultAsync(x => x.SurveyId == message.SurveyId, cancellationToken);

                if (survey == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Survey = Constants.NOT_FOUND });
                }

                _context.Surveys.Remove(survey);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
