﻿import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApplicationState } from '../store';
import * as SamplesStore from '../store/Samples';
import { Formik, Field, Form, FormikHelpers } from 'formik';
import { BallScaleMultiple } from 'react-pure-loaders';
import AsyncSelect from 'react-select/async';

// At runtime, Redux will merge together...
type SampleProps =
  SamplesStore.SamplesState // ... state we've requested from the Redux store
  & typeof SamplesStore.actionCreators // ... plus action creators we've requested
  & RouteComponentProps<{ startDateIndex: string }>; // ... plus incoming routing parameters

interface Values {
    email: string;
    companyId: number;
    surveyId: number;
}

interface Option {
    label: string;
    value: number;
}

const promiseOptions = (inputValue: string) => {
    const url = "api/companies";
    return fetch(url)
        .then(response => response.json())
        .then((data) => {
            var result = data.companies.map((option: any) => ({ label: option.name, value: option.companyId }));

            console.log(result);
            return result;
        })
        .catch(err => {
            console.log('Not Found!', err);
        });
};

class ListSamples extends React.PureComponent<SampleProps> {

  // This method is called when the component is first added to the document
  public componentDidMount() {
    this.ensureDataFetched();
  }

  // This method is called when the route parameters change
  public componentDidUpdate() {
    this.ensureDataFetched();
  }

  public render() {
    return (
      <React.Fragment>
        <h1 id="tabelLabel">Best&auml;ll</h1>
        {this.renderSamplesForm()}
        {this.renderSamplesTable()}
        {this.renderPagination()}
      </React.Fragment>
    );
  }

  private ensureDataFetched() {
    const startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
    this.props.requestSamples(startDateIndex);
  }

    private renderSamplesForm() {
        return (
            <Formik
                initialValues={{
                    email: '',
                    surveyId: 1,
                    companyId: 1
                }}
                onSubmit={(values: Values, { setSubmitting }: FormikHelpers<Values>) => {
                    setTimeout(() => {
                        alert(JSON.stringify(values, null, 2));
                        fetch('api/samples', {
                            method: 'POST', headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({ "Sample" : values }, null, 2),
                        }).then((response) => response.json())
                            .then((responseJson) => {
                                //this.props.history.push("/sample");
                        }) 
                       //setSubmitting(false);
                    }, 500);
                }}
                render={() => (
                    <Form>
                        <div className="form-row mb-4">
                            <div className="col-4">
                                <label htmlFor="email" className="sr-only">Epost</label>
                                <Field className="form-control" id="email" name="email" placeholder="email@email.se" type="text" />
                            </div>
                            <div className="col-7">
                                <label htmlFor="customer" className="sr-only">Kund</label>
                                <AsyncSelect id="customer" placeholder="Välj kund" name="customer" cacheOptions defaultOptions loadOptions={promiseOptions} />
                            </div>
                            <div className="col-1">
                                <button type="submit" className="btn btn-primary float-right">
                                    Skicka
                                </button>
                            </div>
                         </div>
                    </Form>
                )}
            />  
        );
    }

    private renderSamplesTable() {
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Epost</th>
            <th>Kund</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {this.props.samples.map((sample: SamplesStore.Sample) =>
              <tr key={Math.random()}>
                  <td>{sample.email}</td>
                  <td>{sample.company.name}</td>
                  <td>{sample.status.name}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  private renderPagination() {
    const prevStartDateIndex = (this.props.startDateIndex || 0) - 5;
    const nextStartDateIndex = (this.props.startDateIndex || 0) + 5;

    return (
      <div className="d-flex justify-content-between">
        <Link className='btn btn-outline-secondary btn-sm' to={`/sample/${prevStartDateIndex}`}>F&ouml;reg&aring;ende</Link>
            <BallScaleMultiple
                color={'#123abc'}
                loading={this.props.isLoading}
            />
        <Link className='btn btn-outline-secondary btn-sm' to={`/sample/${nextStartDateIndex}`}>N&auml;sta</Link>
      </div>
    );
  }
}

export default connect(
  (state: ApplicationState) => state.samples, // Selects which state properties are merged into the component's props
  SamplesStore.actionCreators // Selects which action creators are merged into the component's props
)(ListSamples as any);
