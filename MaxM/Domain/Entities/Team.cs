﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaxM.Domain.Entities
{
    public class Team
    {
        [JsonIgnore]
        public int TeamId { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public List<Advisor> Advisors { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
