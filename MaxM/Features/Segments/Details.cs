﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Segments
{
    public class Details
    {
        public class Query : IRequest<SegmentEnvelope>
        {
            public Query(int segmentId)
            {
                SegmentId = segmentId;
            }
            public int SegmentId { get; set; }
        }

        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.SegmentId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Query, SegmentEnvelope>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<SegmentEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var segment = await _context.Segments
                    .FirstOrDefaultAsync(x => x.SegmentId == message.SegmentId, cancellationToken);

                if (segment == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Segment = Constants.NOT_FOUND });
                }

                return new SegmentEnvelope(segment);
            }
        }
    }
}
