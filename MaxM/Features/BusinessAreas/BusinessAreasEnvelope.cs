﻿using System.Collections.Generic;
using MaxM.Domain.Entities;

namespace MaxM.Features.BusinessAreas
{
    public class BusinessAreasEnvelope
    {
        public List<BusinessArea> BusinessAreas { get; set; }
        public int BusinessAreasCount { get; set; }
    }
}
