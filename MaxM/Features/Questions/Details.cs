﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Questions
{
    public class Details
    {
        public class Query : IRequest<QuestionEnvelope>
        {
            public Query(int questionId)
            {
                QuestionId = questionId;
            }
            public int QuestionId { get; set; }
        }

        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.QuestionId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Query, QuestionEnvelope>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<QuestionEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var question = await _context.Questions
                    .FirstOrDefaultAsync(x => x.QuestionId == message.QuestionId, cancellationToken);

                if (question == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Question = Constants.NOT_FOUND });
                }

                return new QuestionEnvelope(question);
            }
        }
    }
}
