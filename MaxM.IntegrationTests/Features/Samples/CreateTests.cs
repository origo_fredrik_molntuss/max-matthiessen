using System;
using System.Linq;
using System.Threading.Tasks;
using MaxM.Features.Samples;
using MaxM.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace MaxM.IntegrationTests.Features.Samples
{
    public class CreateTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Create_Sample()
        {
            var companyCommand = new MaxM.Features.Companies.Create.Command()
            {
                Company = new MaxM.Features.Companies.Create.CompanyData()
                {
                    Name = "Teste",
                    OrganizationNumber = "123456789",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }

            };

            var company = await SampleHelpers.CreateCompany(this, companyCommand);

            var statusCommand = new MaxM.Features.Statuses.Create.Command()
            {
                Status = new MaxM.Features.Statuses.Create.StatusData()
                {
                    Name = "Besvarad"
                }

            };

            var status = await SampleHelpers.CreateStatus(this, statusCommand);

            var surveyCommand = new MaxM.Features.Surveys.Create.Command()
            {
                Survey = new MaxM.Features.Surveys.Create.SurveyData()
                {
                    Name = "Enk�t Ett"
                }

            };

            var survey = await SampleHelpers.CreateSurvey(this, surveyCommand);

            var command = new Create.Command()
            {
                Sample = new Create.SampleData()
                {
                    Email = "teste.testesson@test.com",
                    CompanyId = 1,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }

            };

            var sample = await SampleHelpers.CreateSample(this, command);

            Assert.NotNull(sample);
            Assert.Equal(sample.Email, command.Sample.Email);
        }
    }
}