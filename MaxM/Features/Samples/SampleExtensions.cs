using System.Linq;
using MaxM.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Samples
{
    public static class SampleExtensions
    {
        public static IQueryable<Sample> GetAllData(this DbSet<Sample> samples)
        {
            return samples
                .Include(x => x.Company)
                .Include(x => x.Company.Team)
                .Include(x => x.Survey)
                .Include(x => x.Status)
                .AsNoTracking();
        }
    }
}