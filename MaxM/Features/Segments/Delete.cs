﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Segments
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Command(int segmentId)
            {
                SegmentId = segmentId;
            }
            public int SegmentId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.SegmentId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Command>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command message, CancellationToken cancellationToken)
            {
                var segment = await _context.Segments.FirstOrDefaultAsync(x => x.SegmentId == message.SegmentId, cancellationToken);

                if (segment == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Segment = Constants.NOT_FOUND });
                }

                _context.Segments.Remove(segment);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
