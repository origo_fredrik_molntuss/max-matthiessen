﻿using AutoMapper;

namespace MaxM.Features.Users
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Domain.Entities.Advisor, User>(MemberList.None);
        }
    }
}
