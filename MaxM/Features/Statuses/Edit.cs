﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Statuses
{
    public class Edit
    {
        public class StatusData
        {
            public string Name { get; set; }
            public string OrganizationNumber { get; set; }
            public string Email { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class Command : IRequest<StatusEnvelope>
        {
            public StatusData Status { get; set; }
            public int StatusId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Status).NotNull();
            }
        }

        public class Handler : IRequestHandler<Command, StatusEnvelope>
        {
            private readonly MaxMContext _context;

            public Handler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<StatusEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var status = await _context.Statuses
                    .Where(x => x.StatusId == message.StatusId)
                    .FirstOrDefaultAsync(cancellationToken);

                if (status == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Sample = Constants.NOT_FOUND });
                }

                status.Name = message.Status.Name ?? status.Name;

                await _context.SaveChangesAsync(cancellationToken);

                return new StatusEnvelope(await _context.Statuses
                    .Where(x => x.StatusId == status.StatusId)
                    .FirstOrDefaultAsync(cancellationToken));
            }
        }
    }
}
