﻿using System.Collections.Generic;
using MaxM.Domain.Entities;

namespace MaxM.Features.Statuses
{
    public class StatusesEnvelope
    {
        public List<Status> Statuses { get; set; }
        public int StatusesCount { get; set; }
    }
}
