﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface CustomersState {
    isLoading: boolean;
    startDateIndex?: number;
    customers: Customer[];
}

export interface Customer {
    date: string;
    name: string;
    description: string;
    summary: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestCustomersAction {
    type: 'REQUEST_CUSTOMERS';
    startDateIndex: number;
}

interface ReceiveCustomersAction {
    type: 'RECEIVE_CUSTOMERS';
    startDateIndex: number;
    customers: Customer[];
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestCustomersAction | ReceiveCustomersAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestCustomers: (startDateIndex: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.customers && startDateIndex !== appState.customers.startDateIndex) {
            fetch(`customer`)
                .then(response => response.json() as Promise<Customer[]>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_CUSTOMERS', startDateIndex: startDateIndex, customers: data });
                });

            dispatch({ type: 'REQUEST_CUSTOMERS', startDateIndex: startDateIndex });
        }
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: CustomersState = { customers: [], isLoading: false };

export const reducer: Reducer<CustomersState> = (state: CustomersState | undefined, incomingAction: Action): CustomersState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_CUSTOMERS':
            return {
                startDateIndex: action.startDateIndex,
                customers: state.customers,
                isLoading: true
            };
        case 'RECEIVE_CUSTOMERS':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.startDateIndex === state.startDateIndex) {
                return {
                    startDateIndex: action.startDateIndex,
                    customers: action.customers,
                    isLoading: false
                };
            }
            break;
    }

    return state;
};
