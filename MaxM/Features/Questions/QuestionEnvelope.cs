﻿using MaxM.Domain.Entities;

namespace MaxM.Features.Questions
{
    public class QuestionEnvelope
    {
        public Question Question { get; }

        public QuestionEnvelope(Question question)
        {
            Question = question;
        }
    }
}
