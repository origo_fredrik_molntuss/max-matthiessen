﻿using FluentValidation;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MaxM.Features.BusinessAreas
{
    public class Create
    {
        public class BusinessAreaData
        {
            public string Name { get; set; }
        }

        public class BusinessAreaDataValidator : AbstractValidator<BusinessAreaData>
        {
            public BusinessAreaDataValidator()
            {
                RuleFor(x => x.Name).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<BusinessAreaEnvelope>
        {
            public BusinessAreaData BusinessArea { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.BusinessArea).NotNull().SetValidator(new BusinessAreaDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, BusinessAreaEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public Handler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<BusinessAreaEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var businessArea = new BusinessArea()
                {
                    Name = message.BusinessArea.Name,
                };

                await _context.BusinessAreas.AddAsync(businessArea, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return new BusinessAreaEnvelope(businessArea);
            }
        }
    }
}
