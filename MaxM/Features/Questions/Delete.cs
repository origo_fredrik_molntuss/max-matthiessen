﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Questions
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Command(int questionId)
            {
                QuestionId = questionId;
            }
            public int QuestionId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.QuestionId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Command>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command message, CancellationToken cancellationToken)
            {
                var question = await _context.Questions.FirstOrDefaultAsync(x => x.QuestionId == message.QuestionId, cancellationToken);

                if (question == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Question = Constants.NOT_FOUND });
                }

                _context.Questions.Remove(question);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
