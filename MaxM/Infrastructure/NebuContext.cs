﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaxM.Infrastructure
{
    public class NebuContext
    {
        private readonly DbContext _dbContext;

        public NebuContext(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<TEntity> Set<TEntity>() where TEntity : class
        {
            return _dbContext.Set<TEntity>().AsNoTracking();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
