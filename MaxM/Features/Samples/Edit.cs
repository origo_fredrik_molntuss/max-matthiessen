using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Samples
{
    public class Edit
    {
        public class SampleData
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public int CompanyId { get; set; }
            public int StatusId { get; set; }
            public int SurveyId { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class Command : IRequest<SampleEnvelope>
        {
            public SampleData Sample { get; set; }
            public int SampleId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Sample).NotNull();
            }
        }

        public class Handler : IRequestHandler<Command, SampleEnvelope>
        {
            private readonly MaxMContext _context;

            public Handler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<SampleEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var sample = await _context.Samples
                    .Where(x => x.SampleId == message.SampleId)
                    .FirstOrDefaultAsync(cancellationToken); 
                
                var company = await _context.Companies.FirstAsync(x => x.CompanyId == message.Sample.CompanyId, cancellationToken);
                var status = await _context.Statuses.FirstAsync(x => x.StatusId == message.Sample.StatusId, cancellationToken);
                var survey = await _context.Surveys.FirstAsync(x => x.SurveyId == message.Sample.SurveyId, cancellationToken);

                if (sample == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Sample = Constants.NOT_FOUND });
                }

                sample.FirstName = message.Sample.FirstName ?? sample.FirstName;
                sample.LastName = message.Sample.LastName ?? sample.LastName;
                sample.Email = message.Sample.Email ?? sample.Email;
                sample.Company = company;
                sample.Status = status;
                sample.Survey = survey;

                await _context.SaveChangesAsync(cancellationToken);

                if (_context.ChangeTracker.Entries().First(x => x.Entity == sample).State == EntityState.Modified)
                {
                    sample.UpdatedAt = DateTime.UtcNow;
                }

                await _context.SaveChangesAsync(cancellationToken);

                return new SampleEnvelope(await _context.Samples.GetAllData()
                    .Where(x => x.SampleId == sample.SampleId)
                    .FirstOrDefaultAsync(cancellationToken));
            }
        }
    }
}
