﻿using System.Threading.Tasks;

namespace MaxM.Infrastructure.Security
{
    public interface IJwtTokenGenerator
    {
        Task<string> CreateToken(string username);
    }
}