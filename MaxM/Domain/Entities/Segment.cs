﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaxM.Domain.Entities
{
    public class Segment
    {
        public int SegmentId { get; set; }
        public string Name { get; set; }
    }
}
