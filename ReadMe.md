Development
1) Clone from repository 
2) Local development uses SQL Lite. Run migrations to create tables. 
3) Do your changes. 
4) Run tests. 
5) Commit and Push to Repository. 
6) Create Pull Request. 
7) Wait for revision. 
8) Celebrate or hang your head in shame depending on revision outcome.

Api
React Components
OrderSample: Places an order for a new sample
List Samples: Lists all current samples ordered
Act: Summary of key questions.
Analyze: Detailed charts