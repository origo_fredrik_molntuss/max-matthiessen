﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Segments
{
    public class List
    {
        public class Query : IRequest<SegmentsEnvelope>
        {
            public Query(int? limit, int? offset)
            {
                Limit = limit;
                Offset = offset;
            }

            public int? Limit { get; }
            public int? Offset { get;  }
        }

        public class QueryHandler : IRequestHandler<Query, SegmentsEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<SegmentsEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<Segment> queryable = _context.Segments;

                var segments = await queryable
                    .OrderBy(x => x.Name)
                    .Skip(message.Offset ?? 0)
                    .Take(message.Limit ?? 20)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new SegmentsEnvelope()
                {
                    Segments = segments,
                    SegmentsCount = segments.Count()
                };
            }
        }
    }
}
