using System;
using System.Linq;
using System.Threading.Tasks;
using MaxM.Features.Companies;
using MaxM.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace MaxM.IntegrationTests.Features.Companies
{
    public class CreateTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Create_Company()
        {
            var command = new Create.Command()
            {
                Company = new Create.CompanyData()
                {
                    Name = "Test",
                    OrganizationNumber = "123456789",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }

            };

            var company = await CompanyHelpers.CreateCompany(this, command);

            Assert.NotNull(company);
            Assert.Equal(company.Name, command.Company.Name);
        }
    }
}