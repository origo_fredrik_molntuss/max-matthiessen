﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.BusinessAreas
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Command(int businessAreaId)
            {
                BusinessAreaId = businessAreaId;
            }
            public int BusinessAreaId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.BusinessAreaId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Command>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command message, CancellationToken cancellationToken)
            {
                var businessArea = await _context.BusinessAreas.FirstOrDefaultAsync(x => x.BusinessAreaId == message.BusinessAreaId, cancellationToken);

                if (businessArea == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { BusinessArea = Constants.NOT_FOUND });
                }

                _context.BusinessAreas.Remove(businessArea);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
