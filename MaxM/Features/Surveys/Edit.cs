﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Surveys
{
    public class Edit
    {
        public class SurveyData
        {
            public string Name { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class Command : IRequest<SurveyEnvelope>
        {
            public SurveyData Survey { get; set; }
            public int SurveyId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Survey).NotNull();
            }
        }

        public class Handler : IRequestHandler<Command, SurveyEnvelope>
        {
            private readonly MaxMContext _context;

            public Handler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<SurveyEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var survey = await _context.Surveys
                    .Where(x => x.SurveyId == message.SurveyId)
                    .FirstOrDefaultAsync(cancellationToken);

                if (survey == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Survey = Constants.NOT_FOUND });
                }

                survey.Name = message.Survey.Name ?? survey.Name;

                await _context.SaveChangesAsync(cancellationToken);

                return new SurveyEnvelope(await _context.Surveys
                    .Where(x => x.SurveyId == survey.SurveyId)
                    .FirstOrDefaultAsync(cancellationToken));
            }
        }
    }
}
