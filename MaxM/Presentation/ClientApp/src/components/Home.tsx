import * as React from 'react';
import { connect } from 'react-redux';
import './Home.css';

const Home = () => (
    <h1>V&auml;lkommen</h1>
);

export default connect()(Home);
