﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface TeamsState {
    isLoading: boolean;
    startDateIndex?: number;
    teams: Team[];
}

export interface Team {
    date: string;
    name: string;
    description: string;
    summary: string;
    result: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestTeamsAction {
    type: 'REQUEST_TEAMS';
    startDateIndex: number;
}

interface RecieveTeamsAction {
    type: 'RECEIVE_TEAMS';
    startDateIndex: number;
    teams: Team[];
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestTeamsAction | RecieveTeamsAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestTeams: (startDateIndex: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.teams && startDateIndex !== appState.teams.startDateIndex) {
            fetch(`team`)
                .then(response => response.json() as Promise<Team[]>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_TEAMS', startDateIndex: startDateIndex, teams: data });
                });

            dispatch({ type: 'REQUEST_TEAMS', startDateIndex: startDateIndex });
        }
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: TeamsState = { teams: [], isLoading: false };

export const reducer: Reducer<TeamsState> = (state: TeamsState | undefined, incomingAction: Action): TeamsState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_TEAMS':
            return {
                startDateIndex: action.startDateIndex,
                teams: state.teams,
                isLoading: true
            };
        case 'RECEIVE_TEAMS':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.startDateIndex === state.startDateIndex) {
                return {
                    startDateIndex: action.startDateIndex,
                    teams: action.teams,
                    isLoading: false
                };
            }
            break;
    }

    return state;
};
