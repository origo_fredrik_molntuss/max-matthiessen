﻿using FluentValidation;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MaxM.Features.Advisors
{
    public class Create
    {
        public class AdvisorData
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class AdvisorDataValidator : AbstractValidator<AdvisorData>
        {
            public AdvisorDataValidator()
            {
                RuleFor(x => x.FirstName).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<AdvisorEnvelope>
        {
            public AdvisorData Advisor { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Advisor).NotNull().SetValidator(new AdvisorDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, AdvisorEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public Handler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<AdvisorEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var advisor = new Advisor()
                {
                    FirstName = message.Advisor.FirstName,
                    LastName = message.Advisor.LastName,
                };

                await _context.Advisors.AddAsync(advisor, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return new AdvisorEnvelope(advisor);
            }
        }
    }
}
