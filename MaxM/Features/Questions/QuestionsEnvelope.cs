﻿using System.Collections.Generic;
using MaxM.Domain.Entities;

namespace MaxM.Features.Questions
{
    public class QuestionsEnvelope
    {
        public List<Question> Questions { get; set; }
        public int QuestionsCount { get; set; }
    }
}
