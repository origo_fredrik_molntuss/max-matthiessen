using System;
using System.Linq;
using System.Threading.Tasks;
using MaxM.Features.Advisors;
using MaxM.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace MaxM.IntegrationTests.Features.Advisors
{
    public class CreateTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Create_Advisors()
        {

            var command = new Create.Command()
            {
                Advisor = new Create.AdvisorData()
                {
                    FirstName = "Teste",
                    LastName = "Testesson",
                    Email = "teste.testesson@test.com",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }

            };

            var sample = await AdvisorHelpers.CreateAdvisor(this, command);

            Assert.NotNull(sample);
            Assert.Equal(sample.FirstName, command.Advisor.FirstName);
        }
    }
}