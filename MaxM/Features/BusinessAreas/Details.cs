﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.BusinessAreas
{
    public class Details
    {
        public class Query : IRequest<BusinessAreaEnvelope>
        {
            public Query(int businessAreaId)
            {
                BusinessAreaId = businessAreaId;
            }
            public int BusinessAreaId { get; set; }
        }

        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.BusinessAreaId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Query, BusinessAreaEnvelope>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<BusinessAreaEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var businessArea = await _context.BusinessAreas
                    .FirstOrDefaultAsync(x => x.BusinessAreaId == message.BusinessAreaId, cancellationToken);

                if (businessArea == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { BusinessArea = Constants.NOT_FOUND });
                }

                return new BusinessAreaEnvelope(businessArea);
            }
        }
    }
}
