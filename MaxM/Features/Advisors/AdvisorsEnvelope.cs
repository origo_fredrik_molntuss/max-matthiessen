﻿using System.Collections.Generic;
using MaxM.Domain.Entities;

namespace MaxM.Features.Advisors
{
    public class AdvisorsEnvelope
    {
        public List<Advisor> Advisors { get; set; }
        public int AdvisorsCount { get; set; }
    }
}
