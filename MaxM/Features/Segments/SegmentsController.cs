﻿using System.Threading.Tasks;
using MaxM.Infrastructure.Security;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MaxM.Features.Segments
{
    [Route("api/segments")]
    public class SegmentsController : Controller
    {
        private readonly IMediator _mediator;

        public SegmentsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<SegmentsEnvelope> Get([FromQuery] int? teamId, [FromQuery] int? limit, [FromQuery] int? offset)
        {
            return await _mediator.Send(new List.Query(limit, offset));
        }

        [HttpGet("{segmentId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<SegmentEnvelope> Get(int statusId)
        {
            return await _mediator.Send(new Details.Query(statusId));
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<SegmentEnvelope> Create([FromBody]Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("{segmentId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<SegmentEnvelope> Edit(int statusId, [FromBody]Edit.Command command)
        {
            command.SegmentId = statusId;
            return await _mediator.Send(command);
        }

        [HttpDelete("{segmentId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task Delete(int statusId)
        {
            await _mediator.Send(new Delete.Command(statusId));
        }
    }
}