﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MaxM.Features.Companies;
using MaxM.Infrastructure;
using MaxM.IntegrationTests;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace MaxM.IntegrationTests.Features.Companies
{
    public class EditTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Edit_Company()
        {
            var createCommand = new Create.Command()
            {
                Company = new Create.CompanyData()
                {
                    Name = "Test",
                    OrganizationNumber = "123456789",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }
            };

            var createdCompany = await CompanyHelpers.CreateCompany(this, createCommand);

            var command = new Edit.Command()
            {
                Company = new Edit.CompanyData()
                {
                    Name = "Test",
                    OrganizationNumber = "123456789",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                },
                CompanyId = createdCompany.CompanyId
            };

            var dbContext = GetDbContext();

            var companyEditHandler = new Edit.Handler(dbContext);
            var edited = await companyEditHandler.Handle(command, new System.Threading.CancellationToken());

            Assert.NotNull(edited);
            Assert.Equal(edited.Company.Name, command.Company.Name);
        }
    }
}
