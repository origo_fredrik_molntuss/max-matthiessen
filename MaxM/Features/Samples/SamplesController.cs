using System.Threading.Tasks;
using MaxM.Infrastructure.Security;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MaxM.Features.Samples
{
    [Route("api/samples")]
    public class SamplesController : Controller
    {
        private readonly IMediator _mediator;

        public SamplesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<SamplesEnvelope> Get([FromQuery] int? companyId, [FromQuery] int? statusId, [FromQuery] int? limit, [FromQuery] int? offset)
        {
            return await _mediator.Send(new List.Query(companyId, statusId, limit, offset));
        }

        [HttpGet("{sampleId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<SampleEnvelope> Get(int sampleId)
        {
            return await _mediator.Send(new Details.Query(sampleId));
        }

        [HttpPost]
        //[Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<SampleEnvelope> Create([FromBody]Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("{sampleId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<SampleEnvelope> Edit(int sampleId, [FromBody]Edit.Command command)
        {
            command.SampleId = sampleId;
            return await _mediator.Send(command);
        }

        [HttpDelete("{sampleId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task Delete(int sampleId)
        {
            await _mediator.Send(new Delete.Command(sampleId));
        }
    }
}