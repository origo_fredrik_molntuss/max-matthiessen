﻿using FluentValidation;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MaxM.Features.Statuses
{
    public class Create
    {
        public class StatusData
        {
            public string Name { get; set; }
        }

        public class StatusDataValidator : AbstractValidator<StatusData>
        {
            public StatusDataValidator()
            {
                RuleFor(x => x.Name).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<StatusEnvelope>
        {
            public StatusData Status { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Status).NotNull().SetValidator(new StatusDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, StatusEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public Handler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<StatusEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var status = new Status()
                {
                    Name = message.Status.Name,
                };

                await _context.Statuses.AddAsync(status, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return new StatusEnvelope(status);
            }
        }
    }
}
