﻿using FluentValidation;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MaxM.Features.Surveys
{
    public class Create
    {
        public class SurveyData
        {
            public string Name { get; set; }
        }

        public class SurveyDataValidator : AbstractValidator<SurveyData>
        {
            public SurveyDataValidator()
            {
                RuleFor(x => x.Name).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<SurveyEnvelope>
        {
            public SurveyData Survey { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Survey).NotNull().SetValidator(new SurveyDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, SurveyEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public Handler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<SurveyEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var survey = new Survey()
                {
                    Name = message.Survey.Name,
                };

                await _context.Surveys.AddAsync(survey, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return new SurveyEnvelope(survey);
            }
        }
    }
}
