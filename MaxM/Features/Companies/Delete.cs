﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Companies
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Command(int companyId)
            {
                CompanyId = companyId;
            }
            public int CompanyId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.CompanyId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Command>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command message, CancellationToken cancellationToken)
            {
                var company = await _context.Companies.FirstOrDefaultAsync(x => x.CompanyId == message.CompanyId, cancellationToken);

                if (company == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Company = Constants.NOT_FOUND });
                }

                _context.Companies.Remove(company);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
