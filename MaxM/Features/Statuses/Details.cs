﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Statuses
{
    public class Details
    {
        public class Query : IRequest<StatusEnvelope>
        {
            public Query(int statusId)
            {
                StatusId = statusId;
            }
            public int StatusId { get; set; }
        }

        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.StatusId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Query, StatusEnvelope>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<StatusEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var status = await _context.Statuses
                    .FirstOrDefaultAsync(x => x.StatusId == message.StatusId, cancellationToken);

                if (status == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Status = Constants.NOT_FOUND });
                }

                return new StatusEnvelope(status);
            }
        }
    }
}
