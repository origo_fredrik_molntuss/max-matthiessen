﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Microsoft.EntityFrameworkCore;
using MaxM.Features.Samples;
using MaxM.IntegrationTests.Features.Users;
using System;

namespace MaxM.IntegrationTests.Features.Samples
{
    public class DeleteTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Delete_Sample()
        {
            var companyCommand = new MaxM.Features.Companies.Create.Command()
            {
                Company = new MaxM.Features.Companies.Create.CompanyData()
                {
                    Name = "Teste",
                    OrganizationNumber = "123456789",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }

            };

            var company = await SampleHelpers.CreateCompany(this, companyCommand);

            var statusCommand = new MaxM.Features.Statuses.Create.Command()
            {
                Status = new MaxM.Features.Statuses.Create.StatusData()
                {
                    Name = "Besvarad"
                }

            };

            var status = await SampleHelpers.CreateStatus(this, statusCommand);

            var surveyCommand = new MaxM.Features.Surveys.Create.Command()
            {
                Survey = new MaxM.Features.Surveys.Create.SurveyData()
                {
                    Name = "Enkät Ett"
                }

            };

            var survey = await SampleHelpers.CreateSurvey(this, surveyCommand);

            var createCmd = new Create.Command()
            {
                Sample = new Create.SampleData()
                {
                    FirstName = "Teste",
                    LastName = "Testesson",
                    Email = "test@test.com",
                    CompanyId = company.CompanyId,
                    StatusId = status.StatusId,
                    SurveyId = survey.SurveyId,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }
            };

            var sample = await SampleHelpers.CreateSample(this, createCmd);

            var deleteCmd = new Delete.Command(sample.SampleId);

            var dbContext = GetDbContext();

            var sampleDeleteHandler = new Delete.QueryHandler(dbContext);
            await sampleDeleteHandler.Handle(deleteCmd, new System.Threading.CancellationToken());

            var dbSample = await ExecuteDbContextAsync(db => db.Samples.Where(d => d.SampleId == deleteCmd.SampleId).SingleOrDefaultAsync());

            Assert.Null(dbSample);
        }
    }
}
