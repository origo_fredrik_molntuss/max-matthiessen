﻿using MaxM.Domain.Entities;

namespace MaxM.Features.Advisors
{
    public class AdvisorEnvelope
    {
        public Advisor Advisor { get; }

        public AdvisorEnvelope(Advisor advisor)
        {
            Advisor = advisor;
        }
    }
}
