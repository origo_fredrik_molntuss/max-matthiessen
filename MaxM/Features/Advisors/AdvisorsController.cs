﻿using System.Threading.Tasks;
using MaxM.Infrastructure.Security;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MaxM.Features.Advisors
{
    [Produces("application/json")]
    [Route("api/advisors")]
    [ApiController]
    public class AdvisorsController : Controller
    {
        private readonly IMediator _mediator;

        public AdvisorsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Creates a TodoItem.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///        "id": 1,
        ///        "name": "Item1",
        ///        "isComplete": true
        ///     }
        ///
        /// </remarks>
        /// <param name="teamId"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>A newly created TodoItem</returns>
        /// <response code="200">Returns the newly created item</response>
        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<AdvisorsEnvelope> Get([FromQuery] int? teamId, [FromQuery] int? limit, [FromQuery] int? offset)
        {
            return await _mediator.Send(new List.Query(teamId, limit, offset));
        }

        [HttpGet("{advisorId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<AdvisorEnvelope> Get(int advisorId)
        {
            return await _mediator.Send(new Details.Query(advisorId));
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<AdvisorEnvelope> Create([FromBody]Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("{advisorId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<AdvisorEnvelope> Edit(int advisorId, [FromBody]Edit.Command command)
        {
            command.AdvisorId = advisorId;
            return await _mediator.Send(command);
        }

        [HttpDelete("{advisorId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task Delete(int advisorId)
        {
            await _mediator.Send(new Delete.Command(advisorId));
        }
    }
}