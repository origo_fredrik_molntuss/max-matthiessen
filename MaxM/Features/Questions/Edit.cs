﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Questions
{
    public class Edit
    {
        public class QuestionData
        {
            public string Text { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class Command : IRequest<QuestionEnvelope>
        {
            public QuestionData Question { get; set; }
            public int QuestionId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Question).NotNull();
            }
        }

        public class Handler : IRequestHandler<Command, QuestionEnvelope>
        {
            private readonly MaxMContext _context;

            public Handler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<QuestionEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var question = await _context.Questions
                    .Where(x => x.QuestionId == message.QuestionId)
                    .FirstOrDefaultAsync(cancellationToken);

                if (question == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Question = Constants.NOT_FOUND });
                }

                question.Text = message.Question.Text ?? question.Text;

                await _context.SaveChangesAsync(cancellationToken);

                return new QuestionEnvelope(await _context.Questions
                    .Where(x => x.QuestionId == question.QuestionId)
                    .FirstOrDefaultAsync(cancellationToken));
            }
        }
    }
}
