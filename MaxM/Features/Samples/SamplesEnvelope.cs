using System.Collections.Generic;
using MaxM.Domain.Entities;

namespace MaxM.Features.Samples
{
    public class SamplesEnvelope
    {
        public List<Sample> Samples { get; set; }

        public int SamplesCount { get; set; }
    }
}