﻿using System.Collections.Generic;
using MaxM.Domain.Entities;

namespace MaxM.Features.Companies
{
    public class CompaniesEnvelope
    {
        public List<Company> Companies { get; set; }
        public int CompaniesCount { get; set; }
    }
}
