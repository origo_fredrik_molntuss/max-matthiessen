﻿using MaxM.Domain.Entities;

namespace MaxM.Features.Statuses
{
    public class StatusEnvelope
    {
        public Status Status { get; }

        public StatusEnvelope(Status status)
        {
            Status = status;
        }
    }
}
