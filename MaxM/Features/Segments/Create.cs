﻿using FluentValidation;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MaxM.Features.Segments
{
    public class Create
    {
        public class SegmentData
        {
            public string Name { get; set; }
        }

        public class SegmentDataValidator : AbstractValidator<SegmentData>
        {
            public SegmentDataValidator()
            {
                RuleFor(x => x.Name).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<SegmentEnvelope>
        {
            public SegmentData Segment { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Segment).NotNull().SetValidator(new SegmentDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, SegmentEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public Handler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<SegmentEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var segment = new Segment()
                {
                    Name = message.Segment.Name,
                };

                await _context.Segments.AddAsync(segment, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return new SegmentEnvelope(segment);
            }
        }
    }
}
