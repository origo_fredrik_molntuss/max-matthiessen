﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Surveys
{
    public class List
    {
        public class Query : IRequest<SurveysEnvelope>
        {
            public Query(int? teamId, int? limit, int? offset)
            {
                TeamId = teamId;
                Limit = limit;
                Offset = offset;
            }

            public int? TeamId { get; set; }
            public int? Limit { get; }
            public int? Offset { get;  }
        }

        public class QueryHandler : IRequestHandler<Query, SurveysEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<SurveysEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<Survey> queryable = _context.Surveys;

                var surveys = await queryable
                    .OrderBy(x => x.Name)
                    .Skip(message.Offset ?? 0)
                    .Take(message.Limit ?? 20)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new SurveysEnvelope()
                {
                    Surveys = surveys,
                    SurveysCount = surveys.Count()
                };
            }
        }
    }
}
