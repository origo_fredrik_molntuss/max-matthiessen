﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.BusinessAreas
{
    public class List
    {
        public class Query : IRequest<BusinessAreasEnvelope>
        {
            public Query(int? limit, int? offset)
            {
                Limit = limit;
                Offset = offset;
            }

            public int? Limit { get; }
            public int? Offset { get;  }
        }

        public class QueryHandler : IRequestHandler<Query, BusinessAreasEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<BusinessAreasEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<BusinessArea> queryable = _context.BusinessAreas;

                var businessAreas = await queryable
                    .OrderBy(x => x.Name)
                    .Skip(message.Offset ?? 0)
                    .Take(message.Limit ?? 20)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new BusinessAreasEnvelope()
                {
                    BusinessAreas = businessAreas,
                    BusinessAreasCount = businessAreas.Count()
                };
            }
        }
    }
}
