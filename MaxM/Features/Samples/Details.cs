using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Samples
{
    public class Details
    {
        public class Query : IRequest<SampleEnvelope>
        {
            public Query(int sampleId)
            {
                SampleId = sampleId;
            }

            public int SampleId { get; set; }
        }

        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.SampleId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Query, SampleEnvelope>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<SampleEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var sample = await _context.Samples.GetAllData()
                    .FirstOrDefaultAsync(x => x.SampleId == message.SampleId, cancellationToken);

                if (sample == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Sample = Constants.NOT_FOUND });
                }
                return new SampleEnvelope(sample);
            }
        }
    }
}