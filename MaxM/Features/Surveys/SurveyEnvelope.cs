﻿using MaxM.Domain.Entities;

namespace MaxM.Features.Surveys
{
    public class SurveyEnvelope
    {
        public Survey Survey { get; }

        public SurveyEnvelope(Survey survey)
        {
            Survey = survey;
        }
    }
}
