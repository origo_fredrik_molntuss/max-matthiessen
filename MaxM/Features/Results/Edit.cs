﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Results
{
    public class Edit
    {
        public class ResultData
        {
            public string Text { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class Command : IRequest<ResultEnvelope>
        {
            public ResultData Result { get; set; }
            public int ResultId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Result).NotNull();
            }
        }

        public class Handler : IRequestHandler<Command, ResultEnvelope>
        {
            private readonly MaxMContext _context;

            public Handler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<ResultEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var result = await _context.Results
                    .Where(x => x.ResultId == message.ResultId)
                    .FirstOrDefaultAsync(cancellationToken);

                if (result == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Result = Constants.NOT_FOUND });
                }

                result.Text = message.Result.Text ?? result.Text;

                await _context.SaveChangesAsync(cancellationToken);

                return new ResultEnvelope(await _context.Results
                    .Where(x => x.ResultId == result.ResultId)
                    .FirstOrDefaultAsync(cancellationToken));
            }
        }
    }
}
