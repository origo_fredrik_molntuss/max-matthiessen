﻿using System.Threading.Tasks;
using MaxM.Infrastructure.Security;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MaxM.Features.BusinessAreas
{
    [Route("api/businessAreas")]
    public class BusinessAreasController : Controller
    {
        private readonly IMediator _mediator;

        public BusinessAreasController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<BusinessAreasEnvelope> Get([FromQuery] int? teamId, [FromQuery] int? limit, [FromQuery] int? offset)
        {
            return await _mediator.Send(new List.Query(limit, offset));
        }

        [HttpGet("{businessAreaId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<BusinessAreaEnvelope> Get(int statusId)
        {
            return await _mediator.Send(new Details.Query(statusId));
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<BusinessAreaEnvelope> Create([FromBody]Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("{businessAreaId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task<BusinessAreaEnvelope> Edit(int statusId, [FromBody]Edit.Command command)
        {
            command.BusinessAreaId = statusId;
            return await _mediator.Send(command);
        }

        [HttpDelete("{businessAreaId}")]
        [Authorize(AuthenticationSchemes = JwtIssuerOptions.Schemes)]
        public async Task Delete(int statusId)
        {
            await _mediator.Send(new Delete.Command(statusId));
        }
    }
}