﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Questions
{
    public class List
    {
        public class Query : IRequest<QuestionsEnvelope>
        {
            public Query(int? limit, int? offset)
            {
                Limit = limit;
                Offset = offset;
            }

            public int? Limit { get; }
            public int? Offset { get;  }
        }

        public class QueryHandler : IRequestHandler<Query, QuestionsEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<QuestionsEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<Question> queryable = _context.Questions;

                var questions = await queryable
                    .OrderBy(x => x.QuestionId)
                    .Skip(message.Offset ?? 0)
                    .Take(message.Limit ?? 20)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new QuestionsEnvelope()
                {
                    Questions = questions,
                    QuestionsCount = questions.Count()
                };
            }
        }
    }
}
