﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaxM.Domain.Entities
{
    public class Sample
    {
        public int SampleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Answered { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public Advisor Advisor { get; set; }
        public Company Company { get; set; }
        public Survey Survey { get; set; }
        public Status Status { get; set; }
    }
}
