using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Samples
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Command(int sampleId)
            {
                SampleId = sampleId;
            }

            public int SampleId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.SampleId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Command>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command message, CancellationToken cancellationToken)
            {
                var sample = await _context.Samples
                    .FirstOrDefaultAsync(x => x.SampleId == message.SampleId, cancellationToken);

                if (sample == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Sample = Constants.NOT_FOUND });
                }

                _context.Samples.Remove(sample);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}