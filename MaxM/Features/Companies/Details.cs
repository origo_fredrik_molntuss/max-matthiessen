﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Companies
{
    public class Details
    {
        public class Query : IRequest<CompanyEnvelope>
        {
            public Query(int companyId)
            {
                CompanyId = companyId;
            }
            public int CompanyId { get; set; }
        }

        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.CompanyId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Query, CompanyEnvelope>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<CompanyEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var company = await _context.Companies
                    .FirstOrDefaultAsync(x => x.CompanyId == message.CompanyId, cancellationToken);

                if (company == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Company = Constants.NOT_FOUND });
                }

                return new CompanyEnvelope(company);
            }
        }
    }
}
