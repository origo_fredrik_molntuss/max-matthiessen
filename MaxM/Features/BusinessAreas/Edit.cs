﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.BusinessAreas
{
    public class Edit
    {
        public class BusinessAreaData
        {
            public string Name { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class Command : IRequest<BusinessAreaEnvelope>
        {
            public BusinessAreaData BusinessArea { get; set; }
            public int BusinessAreaId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.BusinessArea).NotNull();
            }
        }

        public class Handler : IRequestHandler<Command, BusinessAreaEnvelope>
        {
            private readonly MaxMContext _context;

            public Handler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<BusinessAreaEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var businessArea = await _context.BusinessAreas
                    .Where(x => x.BusinessAreaId == message.BusinessAreaId)
                    .FirstOrDefaultAsync(cancellationToken);

                if (businessArea == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { BusinessArea = Constants.NOT_FOUND });
                }

                businessArea.Name = message.BusinessArea.Name ?? businessArea.Name;

                await _context.SaveChangesAsync(cancellationToken);

                return new BusinessAreaEnvelope(await _context.BusinessAreas
                    .Where(x => x.BusinessAreaId == businessArea.BusinessAreaId)
                    .FirstOrDefaultAsync(cancellationToken));
            }
        }
    }
}
