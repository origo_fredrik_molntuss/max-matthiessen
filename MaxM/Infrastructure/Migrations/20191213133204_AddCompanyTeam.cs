﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MaxM.Migrations
{
    public partial class AddCompanyTeam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TeamId",
                table: "Companies",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Companies_TeamId",
                table: "Companies",
                column: "TeamId");

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Teams_TeamId",
                table: "Companies",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "TeamId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Teams_TeamId",
                table: "Companies");

            migrationBuilder.DropIndex(
                name: "IX_Companies_TeamId",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "Companies");
        }
    }
}
