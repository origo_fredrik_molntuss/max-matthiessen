﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Segments
{
    public class Edit
    {
        public class SegmentData
        {
            public string Name { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class Command : IRequest<SegmentEnvelope>
        {
            public SegmentData Segment { get; set; }
            public int SegmentId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Segment).NotNull();
            }
        }

        public class Handler : IRequestHandler<Command, SegmentEnvelope>
        {
            private readonly MaxMContext _context;

            public Handler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<SegmentEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var segment = await _context.Segments
                    .Where(x => x.SegmentId == message.SegmentId)
                    .FirstOrDefaultAsync(cancellationToken);

                if (segment == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Segment = Constants.NOT_FOUND });
                }

                segment.Name = message.Segment.Name ?? segment.Name;

                await _context.SaveChangesAsync(cancellationToken);

                return new SegmentEnvelope(await _context.Segments
                    .Where(x => x.SegmentId == segment.SegmentId)
                    .FirstOrDefaultAsync(cancellationToken));
            }
        }
    }
}
