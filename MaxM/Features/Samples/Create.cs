﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Samples
{
    public class Create
    {
        public class SampleData
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public int CompanyId { get; set; }
            public int? StatusId { get; set; }
            public int? SurveyId { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class SampleDataValidator : AbstractValidator<SampleData>
        {
            public SampleDataValidator()
            {
                RuleFor(x => x.Email).NotNull().NotEmpty();
                RuleFor(x => x.CompanyId).NotNull().NotEmpty();
                RuleFor(x => x.SurveyId).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<SampleEnvelope>
        {
            public SampleData Sample { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Sample).NotNull().SetValidator(new SampleDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, SampleEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public Handler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<SampleEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                //_currentUserAccessor.GetCurrentUsername()
                var advisor = await _context.Advisors.FirstAsync(x => x.AdvisorId == 1, cancellationToken);
                var company = await _context.Companies.FirstAsync(x => x.CompanyId == 1, cancellationToken);
                var status = await _context.Statuses.FirstAsync(x => x.StatusId == 1, cancellationToken);
                var survey = await _context.Surveys.FirstAsync(x => x.SurveyId == 1, cancellationToken);

                var sample = new Sample()
                {
                    Advisor = advisor,
                    Company = company,
                    Status = status,
                    Survey = survey,
                    FirstName = message.Sample.FirstName ?? null,
                    LastName = message.Sample.LastName ?? null,
                    Email = message.Sample.Email,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                };
                await _context.Samples.AddAsync(sample, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return new SampleEnvelope(sample);
            }
        }
    }
}
