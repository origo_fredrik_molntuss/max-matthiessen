﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Results
{
    public class List
    {
        public class Query : IRequest<ResultsEnvelope>
        {
            public Query(int? limit, int? offset)
            {
                Limit = limit;
                Offset = offset;
            }

            public int? Limit { get; }
            public int? Offset { get;  }
        }

        public class QueryHandler : IRequestHandler<Query, ResultsEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<ResultsEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<Result> queryable = _context.Results;

                var results = await queryable
                    .OrderBy(x => x.ResultId)
                    .Skip(message.Offset ?? 0)
                    .Take(message.Limit ?? 20)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new ResultsEnvelope()
                {
                    Results = results,
                    ResultsCount = results.Count()
                };
            }
        }
    }
}
