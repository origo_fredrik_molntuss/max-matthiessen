﻿using MaxM.Domain.Entities;

namespace MaxM.Features.Companies
{
    public class CompanyEnvelope
    {
        public Company Company { get; }

        public CompanyEnvelope(Company company)
        {
            Company = company;
        }
    }
}
