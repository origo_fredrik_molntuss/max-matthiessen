﻿using System;
using System.Threading.Tasks;
using MaxM.Features.Statuses;
using Xunit;

namespace MaxM.IntegrationTests.Features.Statuses
{
    public class EditTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Edit_Status()
        {
            var createCommand = new Create.Command()
            {
                Status = new Create.StatusData()
                {
                    Name = "Besvarad"
                }
            };

            var createdStatus = await StatusHelpers.CreateStatus(this, createCommand);

            var command = new Edit.Command()
            {
                Status = new Edit.StatusData()
                {
                    Name = "Besvarad",
                },
                StatusId = createdStatus.StatusId
            };

            var dbContext = GetDbContext();

            var statusEditHandler = new Edit.Handler(dbContext);
            var edited = await statusEditHandler.Handle(command, new System.Threading.CancellationToken());

            Assert.NotNull(edited);
            Assert.Equal(edited.Status.Name, command.Status.Name);
        }
    }
}
