﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Advisors
{
    public class List
    {
        public class Query : IRequest<AdvisorsEnvelope>
        {
            public Query(int? teamId, int? limit, int? offset)
            {
                TeamId = teamId;
                Limit = limit;
                Offset = offset;
            }

            public int? TeamId { get; set; }
            public int? Limit { get; }
            public int? Offset { get;  }
        }

        public class QueryHandler : IRequestHandler<Query, AdvisorsEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<AdvisorsEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<Advisor> queryable = _context.Advisors;

                if (_currentUserAccessor.GetCurrentUsername() != null)
                {
                    var currentUser = await _context.Advisors.FirstOrDefaultAsync(x => x.Username == _currentUserAccessor.GetCurrentUsername(), cancellationToken);
                    queryable = queryable.Where(x => x.Team == currentUser.Team);
                }

                var advisors = await queryable
                    .OrderBy(x => x.FirstName)
                    .Skip(message.Offset ?? 0)
                    .Take(message.Limit ?? 20)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new AdvisorsEnvelope()
                {
                    Advisors = advisors,
                    AdvisorsCount = advisors.Count()
                };
            }
        }
    }
}
