﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MaxM.IntegrationTests.Features.Users;

namespace MaxM.IntegrationTests.Features.Advisors
{
    public static class AdvisorHelpers
    {
        /// <summary>
        /// creates an advisor based on the given Create command. It also creates a default user
        /// </summary>
        /// <param name="fixture"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public static async Task<MaxM.Domain.Entities.Advisor> CreateAdvisor(SliceFixture fixture, MaxM.Features.Advisors.Create.Command command)
        {
            // first create the default user
            var user = await UserHelpers.CreateDefaultUser(fixture);

            var dbContext = fixture.GetDbContext();
            var currentAccessor = new StubCurrentUserAccessor(user.Username);

            var advisorCreateHandler = new MaxM.Features.Advisors.Create.Handler(dbContext, currentAccessor);
            var created = await advisorCreateHandler.Handle(command, new System.Threading.CancellationToken());

            var dbSample = await fixture.ExecuteDbContextAsync(db => db.Advisors.Where(s => s.AdvisorId == created.Advisor.AdvisorId)
                .SingleOrDefaultAsync());

            return dbSample;
        }
    }
}
