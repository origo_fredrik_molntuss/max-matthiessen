﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface SamplesState {
    isLoading: boolean;
    startDateIndex?: number;
    samples: Sample[];
}

export interface SamplesEnvelope {
    samples: Sample[];
    samplesCount: number;
}

export interface Team {
    name: string;
}

export interface Company {
    name: string;
    team: Team;
}

export interface Status {
    name: string;
}

export interface Sample {
    company: Company;
    status: Status;
    email: string;
    answered: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestSamplesAction {
    type: 'REQUEST_SAMPLES';
    startDateIndex: number;
}

interface ReceiveSamplesAction {
    type: 'RECEIVE_SAMPLES';
    startDateIndex: number;
    samples: Sample[];
}

interface PostSampleAction {
    type: 'POST_SAMPLE';
    sample: Sample;
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestSamplesAction | ReceiveSamplesAction | PostSampleAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestSamples: (startDateIndex: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.samples && startDateIndex !== appState.samples.startDateIndex) {
            fetch('api/samples')
                .then(response => response.json() as Promise<SamplesEnvelope>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_SAMPLES', startDateIndex: startDateIndex, samples: data.samples });
                });

            dispatch({ type: 'REQUEST_SAMPLES', startDateIndex: startDateIndex });
        }
    },
    postSample: (sample: Sample): AppThunkAction<KnownAction> => (dispatch, getState) => {
        fetch('api/samples',
            {
                method: 'POST'
            })
            .then(response => response.json() as Promise<Sample>)
            .then(data => {
                dispatch({
                    type: 'POST_SAMPLE',
                    sample: sample
                });
            });
            
            
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: SamplesState = { samples: [], isLoading: false };

export const reducer: Reducer<SamplesState> = (state: SamplesState | undefined, incomingAction: Action): SamplesState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_SAMPLES':
            return {
                startDateIndex: action.startDateIndex,
                samples: state.samples,
                isLoading: true
            };
        case 'RECEIVE_SAMPLES':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.startDateIndex === state.startDateIndex) {
                return {
                    startDateIndex: action.startDateIndex,
                    samples: action.samples,
                    isLoading: false
                };
            }
            break;
        case 'POST_SAMPLE':
            return {
                SAMPLE
            };
    }

    return state;
};
