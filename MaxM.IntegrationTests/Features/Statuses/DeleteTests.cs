﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Microsoft.EntityFrameworkCore;
using MaxM.Features.Statuses;
using MaxM.IntegrationTests.Features.Users;
using System;

namespace MaxM.IntegrationTests.Features.Statuses
{
    public class DeleteTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Delete_Status()
        {
            var createCmd = new Create.Command()
            {
                Status = new Create.StatusData()
                {
                    Name = "Besvarad"
                }
            };

            var status = await StatusHelpers.CreateStatus(this, createCmd);

            var deleteCmd = new Delete.Command(status.StatusId);

            var dbContext = GetDbContext();

            var statusDeleteHandler = new Delete.QueryHandler(dbContext);
            await statusDeleteHandler.Handle(deleteCmd, new System.Threading.CancellationToken());

            var dbStatus = await ExecuteDbContextAsync(db => db.Samples.Where(d => d.Status.StatusId == deleteCmd.StatusId).SingleOrDefaultAsync());

            Assert.Null(dbStatus);
        }
    }
}
