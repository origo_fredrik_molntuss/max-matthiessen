﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Advisors
{
    public class Details
    {
        public class Query : IRequest<AdvisorEnvelope>
        {
            public Query(int advisorId)
            {
                AdvisorId = advisorId;
            }
            public int AdvisorId { get; set; }
        }

        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.AdvisorId).NotNull().NotEmpty();
            }
        }

        public class QueryHandler : IRequestHandler<Query, AdvisorEnvelope>
        {
            private readonly MaxMContext _context;

            public QueryHandler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<AdvisorEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var advisor = await _context.Advisors
                    .FirstOrDefaultAsync(x => x.AdvisorId == message.AdvisorId, cancellationToken);

                if (advisor == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Advisor = Constants.NOT_FOUND });
                }

                return new AdvisorEnvelope(advisor);
            }
        }
    }
}
