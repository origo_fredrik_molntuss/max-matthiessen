﻿using System.Collections.Generic;
using MaxM.Domain.Entities;

namespace MaxM.Features.Segments
{
    public class SegmentsEnvelope
    {
        public List<Segment> Segments { get; set; }
        public int SegmentsCount { get; set; }
    }
}
