﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Statuses
{
    public class List
    {
        public class Query : IRequest<StatusesEnvelope>
        {
            public Query(int? teamId, int? limit, int? offset)
            {
                TeamId = teamId;
                Limit = limit;
                Offset = offset;
            }

            public int? TeamId { get; set; }
            public int? Limit { get; }
            public int? Offset { get;  }
        }

        public class QueryHandler : IRequestHandler<Query, StatusesEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public QueryHandler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<StatusesEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                IQueryable<Status> queryable = _context.Statuses;

                var statuses = await queryable
                    .OrderBy(x => x.Name)
                    .Skip(message.Offset ?? 0)
                    .Take(message.Limit ?? 20)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

                return new StatusesEnvelope()
                {
                    Statuses = statuses,
                    StatusesCount = statuses.Count()
                };
            }
        }
    }
}
