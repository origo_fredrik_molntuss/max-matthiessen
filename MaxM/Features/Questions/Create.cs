﻿using FluentValidation;
using MaxM.Domain.Entities;
using MaxM.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MaxM.Features.Questions
{
    public class Create
    {
        public class QuestionData
        {
            public string Text { get; set; }
        }

        public class QuestionDataValidator : AbstractValidator<QuestionData>
        {
            public QuestionDataValidator()
            {
                RuleFor(x => x.Text).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<QuestionEnvelope>
        {
            public QuestionData Question { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Question).NotNull().SetValidator(new QuestionDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, QuestionEnvelope>
        {
            private readonly MaxMContext _context;
            private readonly ICurrentUserAccessor _currentUserAccessor;

            public Handler(MaxMContext context, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<QuestionEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var question = new Question()
                {
                    Text = message.Question.Text,
                };

                await _context.Questions.AddAsync(question, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return new QuestionEnvelope(question);
            }
        }
    }
}
