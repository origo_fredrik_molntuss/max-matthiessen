﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MaxM.Domain;
using MaxM.Infrastructure;
using MaxM.Infrastructure.Errors;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace MaxM.Features.Advisors
{
    public class Edit
    {
        public class AdvisorData
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
        }

        public class Command : IRequest<AdvisorEnvelope>
        {
            public AdvisorData Advisor { get; set; }
            public int AdvisorId { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Advisor).NotNull();
            }
        }

        public class Handler : IRequestHandler<Command, AdvisorEnvelope>
        {
            private readonly MaxMContext _context;

            public Handler(MaxMContext context)
            {
                _context = context;
            }

            public async Task<AdvisorEnvelope> Handle(Command message, CancellationToken cancellationToken)
            {
                var advisor = await _context.Advisors
                    .Where(x => x.AdvisorId == message.AdvisorId)
                    .FirstOrDefaultAsync(cancellationToken);

                if (advisor == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Sample = Constants.NOT_FOUND });
                }

                advisor.FirstName = message.Advisor.FirstName ?? advisor.FirstName;
                advisor.LastName = message.Advisor.LastName ?? advisor.LastName;
                advisor.Email = message.Advisor.Email ?? advisor.Email;

                if (_context.ChangeTracker.Entries().First(x => x.Entity == advisor).State == EntityState.Modified)
                {
                    advisor.UpdatedAt = DateTime.UtcNow;
                }

                await _context.SaveChangesAsync(cancellationToken);

                return new AdvisorEnvelope(await _context.Advisors
                    .Where(x => x.AdvisorId == advisor.AdvisorId)
                    .FirstOrDefaultAsync(cancellationToken));
            }
        }
    }
}
